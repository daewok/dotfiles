############
# Oh my zsh
###########
# Path to your oh-my-zsh installation.
ZSHRC_CANONICAL_LOCATION="$(readlink -f ~/.zshrc)"

export ZSH="$(dirname "$ZSHRC_CANONICAL_LOCATION")/.local/share/ohmyzsh"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to disable command auto-correction.
# DISABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM="$(dirname "$ZSHRC_CANONICAL_LOCATION")/.config/ohmyzsh"

plugins=(git tmux)

ZSH_TMUX_AUTOSTART=true

source $ZSH/oh-my-zsh.sh

export PATH=~/.local/bin:$PATH
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

if which powerline >/dev/null 2>/dev/null; then
  # If powerline is available, try to configure it. Unfortunately, Gentoo
  # doesn't seem to split out the bindings into a separate folder when
  # installing and just leaves them in whatever python site-packages folder the
  # rest is installed in. So we just ask pip to find powerline for us.
  POWERLINE_ROOT="$(pip show powerline-status | grep -e "^Location: " | cut -d" " -f 2)"
  if [ -n "$POWERLINE_ROOT" ]; then
    #POWERLINE_ROOT="/usr/share/powerline"
    POWERLINE_ROOT="$POWERLINE_ROOT/powerline"
    source ${POWERLINE_ROOT}/bindings/zsh/powerline.zsh
  fi

fi

# completion
if which kubectl >/dev/null 2>/dev/null; then
  source <(kubectl completion zsh)
fi

if which helm >/dev/null 2>/dev/null; then
  source <(helm completion zsh)
fi

# aliases

alias cp='cp --reflink=auto'
alias ls='ls --color=tty --quoting-style=literal'
alias kctx='kubectx'
alias kc='kubectl'
alias kns='kubens'

if [[ -e /usr/bin/virtualenvwrapper.sh ]]; then
  source /usr/bin/virtualenvwrapper.sh
fi

function source_ros() {
  if [[ -e "$HOME/catkin_ws/devel/setup.zsh" ]]; then
    source "$HOME/catkin_ws/devel/setup.zsh"
  elif [[ -e /opt/ros/noetic/setup.zsh ]]; then
    source /opt/ros/noetic/setup.zsh
  fi
}


# Local Variables:
# sh-basic-offset: 2
# End:
